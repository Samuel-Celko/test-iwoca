import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'songDurationPipe'
})

/**
 * pipe used to transform seconds into MM:SS format
 */
export class SongDurationPipe implements PipeTransform {

  public transform(value: number): string {
    const minutes: number = Math.floor(value / 60);
    const seconds: number = (value - minutes * 60);
    const secondsString: string = (seconds < 10) ? '0' + seconds.toString() :  seconds.toString();
    return minutes + ':' + secondsString;
  }

}
