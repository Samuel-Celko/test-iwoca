import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {SongsPanelComponent} from './components/songs-panel/songs-panel.component';
import {TopPanelComponent} from './components/top-panel/top-panel.component';
import {BottomPanelComponent} from './components/bottom-panel/bottom-panel.component';
import {SongComponent} from './components/song/song.component';
import {SongDurationPipe} from './pipes/song-duration.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      declarations: [
        AppComponent,
        SongsPanelComponent,
        TopPanelComponent,
        BottomPanelComponent,
        SongComponent,
        SongDurationPipe
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'iwoca-test'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('iwoca-test');
  });

});
