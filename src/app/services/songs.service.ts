import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SongsService {

  /**
   * holds all songs data retrieved from the json
   * @property allSongs
   * @type {ISong[]}
   * @public
   */
  public allSongs: BehaviorSubject<ISong[]>;

  /**
   * true if player is paused
   * @property paused
   * @public
   */
  public paused: BehaviorSubject<boolean>;

  /**
   * currently playing song
   * @property currentlyPlaying
   * @type {ISong}
   * @public
   */
  public currentlyPlaying: BehaviorSubject<ISong>;

  /**
   * relative url to json file with team data
   * @property _jsonUrl
   * @type {string}
   * @private
   */
  private _jsonUrl: string = './assets/songs.json';


  /**
   * js timer
   * @property _timer
   * @type {any}
   * @private
   */
  private _timer: any;

  /**
   * @constructor
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    this.allSongs = new BehaviorSubject<ISong[]>(null);
    this.currentlyPlaying = new BehaviorSubject<ISong>(null);
    this.paused = new BehaviorSubject<boolean>(false);
    this.getSongs();
  }

  /**
   * makes http request to retrieve local json file
   * assigns the result to local _allSongs property
   * @method getSongs
   */
  private getSongs(): void {
    this._httpClient.get(this._jsonUrl).subscribe((songs: ISong[]) => {
      this.allSongs.next(songs);
    }, (error: Error) => {
      console.log(error, 'error getting json data');
    });
  }

  /**
   * updates currently playing property and restarts timer
   * @method playSong
   * @param {ISong} song
   * @public
   */
  public playSong(song: ISong): void {
    this.currentlyPlaying.next({...song});
    this.clearTimer();
    this.startTimer();
  }

  /**
   * stops timer
   * @method pauseSong
   * @public
   */
  public pauseSong(): void {
    this.clearTimer();
  }

  /**
   * resume
   * @method resumeSong
   * @public
   */
  public resumeSong(): void {
    if (this.currentlyPlaying.getValue() !== null) {
      this.startTimer();
    } else {
      this.playSong(this.allSongs.getValue()[0]);
    }
  }

  /**
   * nextSong
   * @method nextSong
   * @public
   */
  public nextSong(): void {
    if (this.currentlyPlaying.getValue() !== null && this.currentSongIndex < this.allSongs.getValue().length - 1) {
      this.playSong(this.allSongs.getValue()[this.currentSongIndex + 1]);
    }
  }

  /**
   * previousSong
   * @method previousSong
   * @public
   */
  public previousSong(): void {
    if (this.currentlyPlaying.getValue() !== null && this.currentSongIndex > 0) {
      this.playSong(this.allSongs.getValue()[this.currentSongIndex - 1]);
    }
  }

  /**
   * clearTimer
   * @method clearTimer
   * @private
   */
  private clearTimer(): void {
    this.paused.next(false);
    clearInterval(this._timer);
  }

  /**
   * starts timer which updates song duration every second
   * @method startTimer
   * @private
   */
  private startTimer(): void {
    this.paused.next(true);
    this._timer = setInterval(() => {
      if (this.currentlyPlaying.getValue().duration > 0) {
        this.currentlyPlaying.getValue().duration--;
        this.currentlyPlaying.next(this.currentlyPlaying.getValue());
      } else {
        this.nextSong();
      }
    }, 1000);
  }

  /**
   * returns index of selected song within all songs - used for next and previous functionality
   * @method currentSongIndex
   * @return {number}
   * @private
   */
  private get currentSongIndex(): number {
    const currentSong: ISong = this.currentlyPlaying.getValue();
    const currentSongIndex: number = this.allSongs.getValue().findIndex(song => song.title === currentSong.title);
    return currentSongIndex;
  }
}

/**
 * type interface for song data
 */
export interface ISong {
  title: string;
  artist: string;
  duration: number;
}

