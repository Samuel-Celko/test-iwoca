import {async, inject, TestBed} from '@angular/core/testing';

import {ISong, SongsService} from './songs.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('SongsService', () => {

  // used to test private properties of the service, otherwise not accessible
  let mockService: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [SongsService]
    });

    mockService = TestBed.get(SongsService) as any;
  });

  it('should be created', () => {
    const service: SongsService = TestBed.get(SongsService);
    expect(service).toBeTruthy();
  });

  it('should have default properties', inject([SongsService], (service: SongsService) => {
    expect(mockService._jsonUrl).toEqual('./assets/songs.json');
  }));

  describe('getSongs', () => {
    it(`should make a http request to retrieve data assign it to _allSongs property`,
      async(
        inject([HttpClient, HttpTestingController], (http: HttpClient, backend: HttpTestingController) => {

          const mockSongs: ISong[] = [
            {artist: 'test', duration: 1, title: 'test'},
            {artist: 'test', duration: 1, title: 'test'},
            {artist: 'test', duration: 1, title: 'test'},
            {artist: 'test', duration: 1, title: 'test'}
          ];

          const req = backend.expectOne({
            url: './assets/songs.json',
            method: 'GET'
          });

          req.flush(mockSongs);

          expect(mockService.allSongs.getValue()).toEqual(mockSongs);
          mockService.getSongs();
        })
      )
    );
  });

  describe('playSong', () => {
    it('should update currently playing song with the value of the song passed', inject([SongsService], (service: SongsService) => {
      const mockSong: ISong = {artist: 'test', duration: 1, title: 'test'};
      service.playSong(mockSong);
      expect(service.currentlyPlaying.getValue()).toEqual(mockSong);
    }));
  });

  describe('resumeSong', () => {
    it('should start timer if there is a currently playing song', () => {
      const spy = spyOn(mockService, 'startTimer');
      const mockSong: ISong = {artist: 'test', duration: 1, title: 'test'};
      mockService.currentlyPlaying.next(mockSong);
      mockService.resumeSong();
      expect(spy).toHaveBeenCalled();
    });

    it('should call playSong if there is no currently playing song', () => {
      const spy = spyOn(mockService, 'playSong');
      mockService.currentlyPlaying.next(null);
      const mockSongs: ISong[] = [
        {artist: 'test', duration: 1, title: 'test'},
        {artist: 'test', duration: 1, title: 'test'},
        {artist: 'test', duration: 1, title: 'test'},
        {artist: 'test', duration: 1, title: 'test'}
      ];
      mockService.allSongs.next(mockSongs);
      mockService.resumeSong();
      expect(spy).toHaveBeenCalledWith(mockSongs[0]);
    });
  });

  describe('nextSong', () => {
    it('should call playSong with the next song in the queue', inject([SongsService], (service: SongsService) => {

      const spy = spyOn(mockService, 'playSong');

      const mockSongs: ISong[] = [
        {artist: 'test1', duration: 1, title: 'test1'},
        {artist: 'test2', duration: 1, title: 'test2'},
        {artist: 'test3', duration: 1, title: 'test3'},
        {artist: 'test4', duration: 1, title: 'test4'}
      ];
      const mockSong: ISong = {artist: 'test1', duration: 1, title: 'test1'};
      mockService.currentlyPlaying.next(mockSong);
      mockService.allSongs.next(mockSongs);

      mockService.nextSong();
      expect(spy).toHaveBeenCalledWith(mockSongs[1]);
    }));
  });

  describe('previousSong', () => {
    it('should call playSong with the previous song in the queue', inject([SongsService], (service: SongsService) => {

      const spy = spyOn(mockService, 'playSong');

      const mockSongs: ISong[] = [
        {artist: 'test1', duration: 1, title: 'test1'},
        {artist: 'test2', duration: 1, title: 'test2'},
        {artist: 'test3', duration: 1, title: 'test3'},
        {artist: 'test4', duration: 1, title: 'test4'}
      ];
      const mockSong: ISong = {artist: 'test2', duration: 1, title: 'test2'};
      mockService.currentlyPlaying.next(mockSong);
      mockService.allSongs.next(mockSongs);

      mockService.previousSong();
      expect(spy).toHaveBeenCalledWith(mockSongs[0]);
    }));
  });

  describe('startTimer', () => {
    it('set paused value to true', inject([SongsService], (service: SongsService) => {
      const mockSong: ISong = {artist: 'test2', duration: 1, title: 'test2'};
      mockService.currentlyPlaying.next(mockSong);
      mockService.startTimer();
      expect(mockService.paused.getValue()).toBeTruthy();
    }));
  });

});
