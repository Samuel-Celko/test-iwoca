import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TopPanelComponent } from './components/top-panel/top-panel.component';
import { BottomPanelComponent } from './components/bottom-panel/bottom-panel.component';
import { SongsPanelComponent } from './components/songs-panel/songs-panel.component';
import { SongComponent } from './components/song/song.component';
import {SongsService} from './services/songs.service';
import {HttpClientModule} from '@angular/common/http';
import {SongDurationPipe} from './pipes/song-duration.pipe';

@NgModule({
  declarations: [
    AppComponent,
    TopPanelComponent,
    BottomPanelComponent,
    SongsPanelComponent,
    SongComponent,
    SongDurationPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [SongsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
