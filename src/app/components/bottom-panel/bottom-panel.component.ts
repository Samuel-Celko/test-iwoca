import {Component, OnInit} from '@angular/core';
import {SongsService} from '../../services/songs.service';

@Component({
  selector: 'app-bottom-panel',
  templateUrl: './bottom-panel.component.html',
  styleUrls: ['./bottom-panel.component.scss']
})
export class BottomPanelComponent implements OnInit {

  public paused: boolean = false;

  constructor(private _songsService: SongsService) {
  }

  public ngOnInit(): void {
    this._songsService.paused.subscribe((paused: boolean) => {
      this.paused = paused;
    });
  }

  public play(): void {
    this._songsService.resumeSong();
  }

  public pause(): void {
    this._songsService.pauseSong();
  }

  public next(): void {
    this._songsService.nextSong();
  }

  public previous(): void {
    this._songsService.previousSong();
  }
}
