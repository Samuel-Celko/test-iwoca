import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {BottomPanelComponent} from './bottom-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {SongsService} from '../../services/songs.service';

describe('BottomPanelComponent', () => {
  let component: BottomPanelComponent;
  let fixture: ComponentFixture<BottomPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      declarations: [BottomPanelComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BottomPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default properties', () => {
    expect(component.paused).toBeFalsy();
  });

  it('should subscribe to value of paused property', inject([SongsService], (service: SongsService) => {
    component.ngOnInit();
    service.paused.next(true);
    expect(component.paused).toBeTruthy();
  }));

  it('should call method in SongService when play method is called', inject([SongsService], (service: SongsService) => {
    const spy = spyOn(service, 'resumeSong');
    component.play();
    expect(spy).toHaveBeenCalled();
  }));

  it('should call method in SongService when pause method is called', inject([SongsService], (service: SongsService) => {
    const spy = spyOn(service, 'pauseSong');
    component.pause();
    expect(spy).toHaveBeenCalled();
  }));

  it('should call method in SongService when next method is called', inject([SongsService], (service: SongsService) => {
    const spy = spyOn(service, 'nextSong');
    component.next();
    expect(spy).toHaveBeenCalled();
  }));

  it('should call method in SongService when previous method is called', inject([SongsService], (service: SongsService) => {
    const spy = spyOn(service, 'previousSong');
    component.previous();
    expect(spy).toHaveBeenCalled();
  }));

});
