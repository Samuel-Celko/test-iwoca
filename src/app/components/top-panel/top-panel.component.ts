import {Component, OnDestroy, OnInit} from '@angular/core';
import {ISong, SongsService} from '../../services/songs.service';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'app-top-panel',
  templateUrl: './top-panel.component.html',
  styleUrls: ['./top-panel.component.scss']
})
export class TopPanelComponent implements OnInit, OnDestroy {

  /**
   * currently selected song to show in top panel
   * @property song
   * @type {ISong}
   * @public
   */
  public song: ISong;

  /**
   * holds subscriptions used
   * @property _subscription
   * @private
   */
  private _subscription: Subscription;

  /**
   * @constructor
   * @param {SongsService} _songsService
   */
  constructor(private _songsService: SongsService) {
  }

  /**
   * angular on init life cycle hook
   * @public
   */
  public ngOnInit(): void {
    this._subscription = this._songsService.currentlyPlaying.subscribe((song: ISong) => {
        if (song !== null) {
          this.song = song;
        }
      }
    );
  }

  /**
   * angular on destroy life cycle hook
   * @public
   */
  public ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

}
