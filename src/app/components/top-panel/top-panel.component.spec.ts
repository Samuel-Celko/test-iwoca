import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {TopPanelComponent} from './top-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {SongComponent} from '../song/song.component';
import {SongDurationPipe} from '../../pipes/song-duration.pipe';
import {ISong, SongsService} from '../../services/songs.service';

describe('TopPanelComponent', () => {
  let component: TopPanelComponent;
  let fixture: ComponentFixture<TopPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      declarations: [TopPanelComponent, SongComponent, SongDurationPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should subscribe to value of currentlyPlaying property', inject([SongsService], (service: SongsService) => {
    component.ngOnInit();
    const mockSong: ISong = {artist: 'test', duration: 1, title: 'test'};
    service.currentlyPlaying.next(mockSong);
    expect(component.song).toEqual(mockSong);
  }));
});
