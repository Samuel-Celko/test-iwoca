import {Component, OnDestroy, OnInit} from '@angular/core';
import {ISong, SongsService} from '../../services/songs.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-songs-panel',
  templateUrl: './songs-panel.component.html',
  styleUrls: ['./songs-panel.component.scss']
})
export class SongsPanelComponent implements OnInit, OnDestroy {

  /**
   * list of all songs from SongsService
   * @property songs
   * @type {ISong[]}
   * @public
   */
  public songs: ISong[];

  /**
   * title of selected song
   * @property selectedSongTitle
   * @type {string}
   * @public
   */
  public selectedSongTitle: string;

  /**
   * holds all subscriptions used in this component
   * @property _subscriptions
   * @private
   */
  private _subscriptions: Subscription = new Subscription();

  /**
   * @constructor
   * @param {SongsService} _songsService
   */
  constructor(private _songsService: SongsService) {
  }

  /**
   * angular on init life cycle hook
   * @public
   */
  public ngOnInit(): void {
    this.getSongs();
    this.getCurrentlyPlaying();
  }

  /**
   * subscribes to SongsService to retrieve all songs
   * @method getSongs
   * @private
   */
  private getSongs(): void {
    this._subscriptions.add(this._songsService.allSongs.subscribe((songs: ISong[]) => {
      if (typeof songs !== 'undefined' && songs !== null) {
        this.songs = songs;
      }
    }));
  }

  /**
   * gets currently playing song from SongsService
   * @method getCurrentlyPlaying
   * @private
   */
  private getCurrentlyPlaying(): void {
    this._subscriptions.add(this._songsService.currentlyPlaying.subscribe((song: ISong) => {
      if (song !== null) {
        this.selectedSongTitle = song.title;
      }
    }));
  }

  /**
   * angular on destroy life cycle hook
   * @public
   */
  public ngOnDestroy(): void {
    this._subscriptions.unsubscribe();
  }
}
