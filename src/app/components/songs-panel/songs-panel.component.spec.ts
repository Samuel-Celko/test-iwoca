import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {SongsPanelComponent} from './songs-panel.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {SongComponent} from '../song/song.component';
import {SongDurationPipe} from '../../pipes/song-duration.pipe';
import {ISong, SongsService} from '../../services/songs.service';

describe('SongsPanelComponent', () => {
  let component: SongsPanelComponent;
  let fixture: ComponentFixture<SongsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      declarations: [SongsPanelComponent, SongComponent, SongDurationPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SongsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should subscribe to value of allSongs property when getSongs is called', inject([SongsService], (service: SongsService) => {
    component.ngOnInit(); // calls getSongs
    const mockSongs: ISong[] = [
      {artist: 'test', duration: 1, title: 'test'},
      {artist: 'test', duration: 1, title: 'test'},
      {artist: 'test', duration: 1, title: 'test'},
      {artist: 'test', duration: 1, title: 'test'}
    ];
    service.allSongs.next(mockSongs);
    expect(component.songs).toEqual(mockSongs);
  }));

  it('should subscribe to value of getCurrentlyPlaying property when getCurrentlyPlaying is called', inject([SongsService], (service: SongsService) => {
    component.ngOnInit(); // calls getCurrentlyPlaying
    const mockSong: ISong = {artist: 'test', duration: 1, title: 'test'};

    service.currentlyPlaying.next(mockSong);
    expect(component.selectedSongTitle).toEqual(mockSong.title);
  }));

});
