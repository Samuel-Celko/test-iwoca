import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {ISong, SongsService} from '../../services/songs.service';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.scss']
})
export class SongComponent implements OnChanges {

  /**
   * passed in songData
   * @property songData
   * @type {ISong}
   */
  @Input() songData: ISong;

  /**
   * determines whether the component is displayed in the top panel
   * @property inTopPanel
   * @type {boolean}
   */
  @Input() inTopPanel: boolean = false;

  /**
   * utilised when this component is used in the top panel
   * @property totalSongDuration
   * @type {number}
   */
  public totalSongDuration: number;

  /**
   * @constructor
   * @param {SongsService} _songsService
   */
  constructor(private _songsService: SongsService) {
  }

  /**
   * angular on change life cycle hook
   * @public
   */
  public ngOnChanges(): void {
    const copy: ISong = {...this.songData} as ISong;
    this.totalSongDuration = copy.duration;
  }

  /**
   * component click handler
   * @method playSong
   */
  public playSong(): void {
    this._songsService.playSong(this.songData);

  }
}


