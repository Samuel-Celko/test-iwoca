import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {SongComponent} from './song.component';
import {SongDurationPipe} from '../../pipes/song-duration.pipe';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import {SongsService} from '../../services/songs.service';

describe('SongComponent', () => {
  let component: SongComponent;
  let fixture: ComponentFixture<SongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      declarations: [SongComponent, SongDurationPipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SongComponent);
    component = fixture.componentInstance;
    component.songData = {title: 'test', duration: 1, artist: 'test'};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have default properties', () => {
    expect(component.inTopPanel).toBeFalsy();
  });

  it('should assign value to totalSongDuration on ng change event', () => {
    component.ngOnChanges();
    expect(component.totalSongDuration).toEqual(1);
  });

  it('should call method in SongService when play method is called', inject([SongsService], (service: SongsService) => {
    const spy = spyOn(service, 'playSong');
    component.playSong();
    expect(spy).toHaveBeenCalled();
  }));
});
