# IwocaTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Overview

The purpose of this project is to demonstrate production ready code for a single page application which displays a music player interface.
The view is responsive and optimised for both desktop and mobile.

## Functionality
- play - by clicking the play button or clicking/tapping on a song
- pause - by clicking the pause button
- next - by clicking the next button 
- previous - by clicking the previous button
- automatic play - next song in the queue will start automatically after the previous song is played out
 
## Architecture / Structure

The composition of this project consists of 4 Components and 1 Service - detailed below
For testing, it uses Karma + Jasmine.

### SongsService
- using http request it retrieves all song data from a local json file
- handles player functionality (play, resume, next, previous)
- stores currently playing song data

### TopPanelComponent
- displayed at the top of the app view
- displays currently playing song and elapsed time 

### SongsPanel
- displays all songs in a responsive list

### SongComponent
- single song component
- displays title, artist and duration
- uses a click handler in order to notify the app to play the selected song   

### BottomPanelComponent
- displayed at the bottom of the view 
- displays play, paus, previous and next icons and handles click events 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
